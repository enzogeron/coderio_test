import prompts from 'prompts'
import _ from 'lodash'
import { mostFreqWord, wordsNormalized, formatResult} from './src/functions'

(async () => {

  const questions = [
    {
      type: 'text',
      name: 'paragraph',
      message: 'Paragraph: ',
    },
    {
      type: 'text',
      name: 'wordsNotAllowed',
      message: 'List words not allowed (without spaces separated by ,): ',
    }
  ]

  const response = await prompts(questions)

  const { paragraph, wordsNotAllowed } = response

  const listWordsNotAllowed = response.wordsNotAllowed ? wordsNotAllowed.split(',') : []

  const paragraphClean = wordsNormalized(listWordsNotAllowed, paragraph)

  const result = mostFreqWord(paragraphClean)

  console.log('Output: ', formatResult(result))

})()
