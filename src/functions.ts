import _ from 'lodash'

const wordsSplit = (paragraph: String): Object => {
    const words = paragraph.toLowerCase().replace(/[!?',;.]/g, '').split(/\s/).filter(word => word.length != 0)
    const map = {}
    words.forEach((w) => {
      if (!map[w]) {
        map[w] = 0;
      }
      map[w] += 1;
    });
  
    return map;
  }
  
  const mostFreqWord = (paragraph: String) => {
    const objWords = wordsSplit(paragraph)
    const values = _.fromPairs(_.sortBy(_.toPairs(objWords), 1).reverse())
    return values
  }
  
  const wordsNormalized = (wordsNotAllowed: String[], paragraph: String) => {
    const regExp = new RegExp(wordsNotAllowed.join('|'), 'gi')
  
    const normalized = paragraph.replace(regExp, '')
  
    return normalized
  }

  const formatResult = (objResult) => _.keys(objResult)[0]

  export {
      mostFreqWord,
      wordsNormalized,
      formatResult,
  }