import { mostFreqWord, wordsNormalized, formatResult} from './src/functions'

describe('Tests', () => {
  it('Case 01: ', () => {
    const paragraph = 'Bob hit a ball, the hit BALL flew far after it was hit.'
    const wordsNotAllowed = ['hit']

    const paragraphClean = wordsNormalized(wordsNotAllowed, paragraph)

    const result = mostFreqWord(paragraphClean)

    expect(formatResult(result)).toEqual('ball')

  })

  it('Case 02: ', () => {
    const paragraph = 'a.'
    const wordsNotAllowed = []

    const paragraphClean = wordsNormalized(wordsNotAllowed, paragraph)

    const result = mostFreqWord(paragraphClean)

    expect(formatResult(result)).toEqual('a')

  })

  it('Case 03: ', () => {
    const paragraph = 'Hola mundo, hola.! test prueba prueba prueba hola mundo test test nuevo'
    const wordsNotAllowed = ['prueba', 'hola']

    const paragraphClean = wordsNormalized(wordsNotAllowed, paragraph)

    const result = mostFreqWord(paragraphClean)

    expect(formatResult(result)).toEqual('test')

  })

})