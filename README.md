## Test

Given a string called "paragraph" and a list of strings called "words Not Allowed", it should be
returned the most frequent word within the paragraph that is not in the list of words Not Allowed

- Realizar en javascript o typescript

It is guaranteed that there is only one word as a result and that the word is allowed.

The words within the paragraph are "case-insensitive" and the answer should be in "lowercase".

Example # 1:
Input:
paragraph = "Bob hit a ball, the hit BALL flew far after it was hit.",
wordsNotAllowed = ["hit"]
Output: "ball"
Explain:
"hit" occurs 3 times, but is a disallowed word.

"ball" occurs twice (and no other word occurs in that amount), therefore; is the word most
often.

Take note that the words in the paragraph are not "case sensitive" and that the punctuation
was ignored (For example "ball," and "BALL" end up being the same word)

Example # 2:
Input:
paragraph = "a.",
wordsNotAllowed = []
Output: "a"

Constraints:

1 <= paragraph.length <= 1000
paragraph consists of letters of the alphabet, space '', or any of the following symbols: "!?',;."
0 <= wordsNotAllowed.length <= 100
1 <= wordsNotAllowed[i].length <= 10

wordsNotAllowed[i] is a word in "lowercase" that only contains letters of the alphabet (a-z)
